import cv2

coordinates = []

def Capture_Event(event, x, y, flags, params):
    if event == cv2.EVENT_LBUTTONDOWN:
        coordinates.append((x, y))

if __name__ == "__main__":
    img = cv2.imread('C:\\Users\\jains\\Desktop\\holoworld\\assets\\imgs\\ba7b4486-0294-4824-9e7c-a2aaa89d2ecc-removebg-preview.png', 1)
    img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)
    cv2.imshow('image', img)
    cv2.setMouseCallback('image', Capture_Event)

    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print("Coordinates:", coordinates)
