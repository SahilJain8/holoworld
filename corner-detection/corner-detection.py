import cv2
import matplotlib.pyplot as plt
import numpy as np
input_img = cv2.imread("assets/imgs/ba7b4486-0294-4824-9e7c-a2aaa89d2ecc-removebg-preview.png")



gray_scale = cv2.cvtColor(input_img,cv2.COLOR_RGB2GRAY)





corners = cv2.goodFeaturesToTrack(gray_scale,100,0.2,10)
corners = np.int0(corners)


for corner in corners:
    x,y = corner.ravel()
    cv2.circle(input_img,(x,y),3,(225,0,0),-1)




cv2.imshow("Frame",input_img)

cv2.waitKey(0)
cv2.destroyAllWindows()